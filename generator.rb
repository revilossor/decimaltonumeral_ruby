require 'numeralobjects'
class RomanNumeralGenerator
	def generate(decimal, numeral='')
		if(decimal==0) 
			return numeral 
		end
		obj = NumeralObjects.get(decimal)
		generate(decimal-obj[0],numeral+obj[1])		
	end
end